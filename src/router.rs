use crate::routes;
use gotham::pipeline::new_pipeline;
use gotham::pipeline::single::single_pipeline;
use gotham::router::{builder::*, Router};

pub fn router() -> Router {
    let pipeline = new_pipeline().build();

    let (chain, pipelines) = single_pipeline(pipeline);

    build_router(chain, pipelines, |route| {
        route.get("/").to(routes::root);
        route
            .get("/kmeans")
            .with_query_string_extractor::<routes::KmeansExtractor>()
            .to(routes::cluster_kmeans);
        route.get("/hierarchical").to(routes::cluster_hierarchical);
    })
}
