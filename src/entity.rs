use std::fmt;

#[derive(Debug, PartialEq, Eq, Hash, Serialize)]
pub struct Blog {
    pub name: String,
    pub words: Vec<String>,
    pub word_counts: Vec<u64>,
}

#[derive(Debug, Serialize)]
pub struct Centroid<'a> {
    pub word_counts: Vec<u64>,
    assignments: Vec<&'a Blog>,
}

impl<'a> Centroid<'a> {
    pub fn new(word_counts: Vec<u64>) -> Self {
        Centroid {
            word_counts,
            assignments: vec![],
        }
    }

    pub fn add(&mut self, blog: &'a Blog) {
        self.assignments.push(blog);
    }

    pub fn clear_assignments(&mut self) {
        self.assignments.clear();
    }

    pub fn assignments(&self) -> &[&Blog] {
        &self.assignments
    }
}

impl fmt::Display for Centroid<'_> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        for blog in self.assignments.iter() {
            write!(f, "{}, ", blog.name)?;
        }
        Ok(())
    }
}
