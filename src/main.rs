extern crate gotham;
#[macro_use]
extern crate gotham_derive;
extern crate hyper;
extern crate itertools;
extern crate mime;
extern crate rand;
#[macro_use]
extern crate tera;
#[macro_use]
extern crate lazy_static;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;

mod entity;
mod extensions;
mod hierarchical;
mod k_means;
mod parser;
mod router;
mod routes;
mod stats;

fn main() {
    let addr = "127.0.0.1:8080";
    println!("Listening for requests at http://{}", addr);
    gotham::start(addr, router::router());
}
