use self::Cluster::*;
use crate::entity::Blog;
use crate::stats;
use itertools::Itertools;
use std::borrow::Borrow;
use std::cmp;
use std::rc::Rc;

#[derive(Debug, PartialEq)]
pub enum Cluster {
    Leaf(&'static Blog),
    Node {
        left: Box<Cluster>,
        right: Box<Cluster>,
        parent: Option<Rc<Cluster>>,
        word_counts: Vec<u64>,
        distance: f64,
    },
}

impl Cluster {
    fn word_counts(&self) -> &'_ [u64] {
        match self {
            Leaf(blog) => blog.word_counts.as_ref(),
            Node {
                ref word_counts, ..
            } => word_counts,
        }
    }
}

pub fn cluster(blogs: &'static [Blog]) -> Cluster {
    assert!(!blogs.is_empty(), "blogs cannot be empty");

    let mut clusters = blogs.into_iter().map(|b| Leaf(b)).collect_vec();

    while clusters.len() > 1 {
        iterate(&mut clusters);
    }

    clusters.remove(0)
}

struct WithIndex<'a, T> {
    index: usize,
    item: &'a T,
}

fn iterate(clusters: &mut Vec<Cluster>) {
    let with_indices = clusters
        .iter()
        .enumerate()
        .map(|e| WithIndex {
            index: e.0,
            item: e.1,
        }).collect_vec();

    let closest_indices = pair_combinations(&with_indices)
        .into_iter()
        .map(|comb| {
            (
                ((comb.0).index, (comb.1).index),
                stats::pearson((comb.0).item.word_counts(), (comb.1).item.word_counts()),
            )
        }).fold1(|a, b| if a.1 > b.1 { a } else { b })
        .unwrap();

    let ((index_a, index_b), distance) = closest_indices;

    // If we always remove the Cluster at the larger index first, the Cluster at the
    // smaller index will not change it's position in the vector.
    let closest_a = clusters.remove(cmp::max(index_a, index_b));
    let closest_b = clusters.remove(cmp::min(index_a, index_b));

    let new_cluster = merge(closest_a, closest_b, distance);

    clusters.push(new_cluster);
}

fn merge(a: Cluster, b: Cluster, distance: f64) -> Cluster {
    let word_counts = average(a.word_counts(), b.word_counts());

    Cluster::Node {
        left: Box::new(a),
        right: Box::new(b),
        parent: None,
        word_counts,
        distance,
    }
}

fn pair_combinations<T, Q>(list: &[Q]) -> Vec<(&T, &T)>
where
    Q: Borrow<T>,
{
    let mut list = list.into_iter().map(|item| item.borrow()).collect_vec();
    let mut ret = vec![];

    while list.len() > 1 {
        let first = list.remove(0);
        for elem in &list {
            ret.push((first, *elem));
        }
    }

    ret
}

fn average(a: &[u64], b: &[u64]) -> Vec<u64> {
    assert_eq!(a.len(), b.len());

    let mut ret = vec![0; a.len()];
    for index in 0..a.len() {
        let combined = a[index] + b[index];
        let avg = combined as f64 / 2.0;
        ret[index] = avg.round() as u64;
    }
    ret
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_pair_combinations() {
        assert_eq!(
            pair_combinations(&['a', 'b', 'c']),
            vec![(&'a', &'b'), (&'a', &'c'), (&'b', &'c')]
        );
        assert_eq!(pair_combinations(&['a', 'b']), vec![(&'a', &'b')]);
        assert_eq!(pair_combinations(&['a']), vec![]);
        assert_eq!(pair_combinations(&[] as &[char]), vec![]);
    }
}
