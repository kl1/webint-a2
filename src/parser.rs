use crate::entity::Blog;
use crate::extensions::*;
use std::fs::File;
use std::io::BufRead;
use std::io::BufReader;
use std::path::Path;

pub fn parse_blogs<P: AsRef<Path>>(path: P) -> AnyResult<Vec<Blog>> {
    let file = File::open(path)?;
    let mut line_iter = BufReader::new(file).lines();

    // Get the words form the first line in the file
    let words: Vec<String> = line_iter
        .next()
        .ok_or("failed to read header line")??
        .split("\t")
        .skip(1)
        .map(|l| l.to_string())
        .collect();

    // Parse and collect blog data
    line_iter
        .map(|line| -> AnyResult<Blog> {
            let line = line?;
            let mut fields = line.split("\t");
            let blog_name = fields.next().ok_or("failed to read field")?;
            let mut word_counts = vec![];
            for field in fields {
                word_counts.push(field.parse()?);
            }

            Ok(Blog {
                name: blog_name.to_string(),
                words: words.clone(),
                word_counts,
            })
        }).fold_results_vec()
}
