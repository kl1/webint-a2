use crate::entity::Blog;
use crate::entity::Centroid;
use crate::stats;
use itertools::Itertools;
use rand::Rng;
use std::cmp;

pub struct Clustering<'a> {
    /// The centroids for the clustering
    pub centroids: Vec<Centroid<'a>>,
    /// The number of iterations taken
    pub iteration_count: usize,
}

/// Clusters a given amount of iterations
pub fn cluster_iterations(blogs: &[Blog], k: usize, iterations: usize) -> Clustering {
    cluster(blogs, k, iterations, false)
}

/// Clusters until no more changes are made
pub fn cluster_optimal(blogs: &[Blog], k: usize) -> Clustering {
    cluster(blogs, k, 0, true)
}

fn cluster(blogs: &[Blog], k: usize, iterations: usize, optimal: bool) -> Clustering {
    let mut centroids = make_centroids(&blogs, k);
    let mut iter_count: usize = 0;

    loop {
        iter_count += 1;
        println!("cluster iter: {}", iter_count);

        // Clear current centroid assignments
        centroids.iter_mut().for_each(Centroid::clear_assignments);

        // Add each blog to the closest centroid
        for blog in blogs.iter() {
            let mut distances = centroids
                .iter_mut()
                .map(|centroid| {
                    let distance = stats::pearson(&blog.word_counts, &centroid.word_counts);
                    (centroid, distance)
                }).sorted_by(|a, b| a.1.partial_cmp(&b.1).unwrap().reverse());

            distances
                .first_mut()
                .expect("centroids cannot be empty")
                .0
                .add(blog);
        }

        // Is set to true if any centroid changes it's position in this iteration
        let mut change = false;

        // Re-calculate center for each centroid
        for centroid in centroids.iter_mut() {
            // If we don't have any blogs don't attempt to recalculate center
            if centroid.assignments().is_empty() {
                continue;
            }

            let new_center: Vec<u64> = centroid
                .assignments()
                .into_iter()
                .fold(
                    vec![0 as u64; centroid.word_counts.len()],
                    |mut acc, assign| {
                        for (i, count) in assign.word_counts.iter().enumerate() {
                            acc[i] += count;
                        }
                        acc
                    },
                ).into_iter()
                .map(|count| {
                    if count == 0 {
                        0
                    } else {
                        (count as f64 / centroid.assignments().len() as f64).round() as u64
                    }
                }).collect();

            if optimal && (new_center != centroid.word_counts) {
                change = true;
            }

            centroid.word_counts = new_center;
        }

        if (optimal && !change) || (!optimal && (iter_count >= iterations)) {
            break;
        }
    }

    Clustering {
        centroids,
        iteration_count: iter_count,
    }
}

/// For a given set of blogs, generates k centroids.
fn make_centroids(blogs: &[Blog], k: usize) -> Vec<Centroid> {
    assert!(!blogs.is_empty(), "blogs cannot be empty");

    let words_len = blogs[0].words.len();

    let mut min_word_counts: Vec<u64> = vec![0; words_len];
    let mut max_word_counts: Vec<u64> = vec![0; words_len];

    for i in 0..words_len {
        for blog in blogs.into_iter() {
            min_word_counts[i] = cmp::min(min_word_counts[i], blog.word_counts[i]);
            max_word_counts[i] = cmp::max(max_word_counts[i], blog.word_counts[i]);
        }
    }

    (0..k)
        .map(|_| {
            let word_counts = (0..words_len)
                .map(|i| {
                    let min = min_word_counts[i];
                    let max = max_word_counts[i];
                    rand::thread_rng().gen_range(min, max + 1)
                }).collect();

            Centroid::new(word_counts)
        }).collect()
}
