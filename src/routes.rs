use crate::entity::Blog;
use crate::parser;
use gotham::helpers::http::response;
use gotham::state::{FromState, State};
use tera::{Context, Tera};

use crate::hierarchical;
use crate::hierarchical::Cluster;
use crate::k_means;
use hyper::{Body, Response, StatusCode};

lazy_static! {
    static ref TERA: Tera = {
        let mut tera = compile_templates!("templates/**/*");
        tera.autoescape_on(vec![]);
        tera
    };
}

lazy_static! {
    static ref BLOGS: Vec<Blog> = { parser::parse_blogs("res/blogdata.txt").unwrap() };
}

// /

pub fn root(state: State) -> (State, Response<Body>) {
    let context = Context::new();
    let html = TERA.render("main.html", &context).unwrap();

    let res = create_html_response(&state, html);
    (state, res)
}

// /kmeans

#[derive(Deserialize, StateData, StaticResponseExtender)]
pub struct KmeansExtractor {
    pub optimal: Option<bool>,
    pub k: usize,
    pub iterations: usize,
}

pub fn cluster_kmeans(mut state: State) -> (State, Response<Body>) {
    let KmeansExtractor {
        optimal,
        k,
        iterations,
    } = KmeansExtractor::take_from(&mut state);
    let optimal = optimal.unwrap_or(false);

    let clustering = if optimal {
        k_means::cluster_optimal(&*BLOGS, k)
    } else {
        k_means::cluster_iterations(&*BLOGS, k, iterations)
    };

    let mut context = Context::new();
    context.insert("blog_count", &BLOGS.len());
    context.insert("k_count", &k);
    context.insert("iter_count", &clustering.iteration_count);
    context.insert("centroids", &clustering.centroids);

    let html = TERA.render("kmeans.html", &context).unwrap();

    let res = create_html_response(&state, html);
    (state, res)
}

// /kmeans

pub fn cluster_hierarchical(state: State) -> (State, Response<Body>) {
    let clustering = hierarchical::cluster(&*BLOGS);

    let clustering_html = build_hierarchical_clustering_html(&clustering);

    let mut context = Context::new();
    context.insert("cluster", &clustering_html);

    let html = TERA.render("hierarchical.html", &context).unwrap();

    let res = create_html_response(&state, html);
    (state, res)
}

fn build_hierarchical_clustering_html(cluster: &Cluster) -> String {
    let mut html = String::new();

    fn build(html: &mut String, cluster: &Cluster) {
        match cluster {
            Cluster::Leaf(blog) => html.push_str(&blog.name),
            Cluster::Node { left, right, .. } => {
                html.push_str("<ul>");

                html.push_str("<li class=\"right\">R→ ");
                build(html, right);
                html.push_str("</li>");

                html.push_str("<li class=\"left\">L→ ");
                build(html, left);
                html.push_str("</li>");


                html.push_str("</ul>");
            }
        }
    }

    build(&mut html, cluster);
    html
}

// Helpers

fn create_html_response<B>(state: &State, body: B) -> Response<Body>
where
    B: Into<Body>,
{
    response::create_response(state, StatusCode::OK, mime::TEXT_HTML, body)
}
