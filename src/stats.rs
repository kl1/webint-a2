pub fn pearson(counts_a: &[u64], counts_b: &[u64]) -> f64 {
    covariance(counts_a, counts_b) / (stddev(counts_a) * stddev(counts_b))
}

// stat helpers

fn mean(numbers: &[u64]) -> f64 {
    numbers.iter().sum::<u64>() as f64 / numbers.len() as f64
}

// Calculates the covariance of two slices of numbers. If the length of the slices are not equal
// the numbers that are present in the longer but not in the shorter slice are ignored.
fn covariance(samples_a: &[u64], samples_b: &[u64]) -> f64 {
    let length = std::cmp::min(samples_a.len(), samples_b.len());
    if length == 0 {
        return 0.0;
    };

    let mean_a = mean(&samples_a);
    let mean_b = mean(&samples_b);

    samples_a
        .iter()
        .zip(samples_b.iter())
        .map(|s| (*s.0 as f64 - mean_a) * (*s.1 as f64 - mean_b))
        .sum::<f64>()
        / length as f64
}

fn stddev(samples: &[u64]) -> f64 {
    let length = samples.len();
    if length == 0 {
        return 0.0;
    }

    let mean_s = mean(&samples);

    let squared_average = samples
        .iter()
        .map(|s| (*s as f64 - mean_s).powf(2.0))
        .sum::<f64>()
        / length as f64;

    squared_average.sqrt()
}
